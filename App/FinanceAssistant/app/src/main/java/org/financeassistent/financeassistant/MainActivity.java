package org.financeassistent.financeassistant;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;

import org.financeassistent.financeassistant.model.Product;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onButtonClick(View view){
        Intent intent = new Intent(this, AddProductActivity.class);
        startActivity(intent);
    }

    public void onButtonClickUpdate(View view) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}