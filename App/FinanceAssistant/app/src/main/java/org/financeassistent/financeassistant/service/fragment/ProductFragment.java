package org.financeassistent.financeassistant.service.fragment;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.financeassistent.financeassistant.AddProductActivity;
import org.financeassistent.financeassistant.MainActivity;
import org.financeassistent.financeassistant.R;
import org.financeassistent.financeassistant.model.Product;
import org.financeassistent.financeassistant.service.typechange.impl.TypeChanger;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

public class ProductFragment extends Fragment {

    private ArrayList<Product> products;
    View view;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.product_fragment, container, false);

        GetProducts getProducts = new GetProducts();
        getProducts.execute();
        try {
            products = getProducts.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        showProducts();

        return view;
    }

    public void showProducts(){
        TableLayout tableLayout = (TableLayout)view.findViewById(R.id.productsTable);
        TypeChanger typeChanger = new TypeChanger();
        String necessary;

        for(final Product product : products)
        {
            TableRow tableRow = new TableRow(tableLayout.getContext());

            TextView textViewName = new TextView(tableRow.getContext());
            textViewName.setText(product.getName());
            textViewName.setTextSize(13);
            textViewName.setPadding(5, 6, 5, 6);
            tableRow.addView(textViewName);

            TextView textViewPrice = new TextView(tableRow.getContext());
            textViewPrice.setText(String.valueOf(product.getPrice()));
            textViewPrice.setPadding(50, 6, 5, 6);
            tableRow.addView(textViewPrice);

            necessary = typeChanger.BoolToString(product.getWasNecessary());
            TextView textViewNecessary = new TextView(tableRow.getContext());
            textViewNecessary.setPadding(50, 6, 5, 6);
            textViewNecessary.setText(necessary);
            tableRow.addView(textViewNecessary);

            final Button button = new Button(tableRow.getContext());
            button.setText("Удалить");
            button.setTextSize(10);
            button.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    DeletProduct deletProduct = new DeletProduct(product);
                    deletProduct.execute();
                }
            });

            tableRow.addView(button);
            tableLayout.addView(tableRow);
        }
    }

    public class GetProducts extends AsyncTask<Void, Void, ArrayList<Product>> {
        @Override
        protected ArrayList<Product> doInBackground(Void... params) {
            ArrayList<Product> products;
            try {
                final String url = "http://10.0.2.2:8080/product";
                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(0,
                        new StringHttpMessageConverter(Charset.forName("UTF-8")));

                ResponseEntity<String> response
                        = restTemplate.getForEntity(url, String.class);

                ObjectMapper mapper = new ObjectMapper();
                JsonNode root = mapper.readTree(response.getBody());
                products = mapper.convertValue(root, new TypeReference<ArrayList<Product>>(){});
//                Product product = mapper.convertValue(root, Product.class);

                return products;
            } catch (Exception e) {
                Log.e("MainActivity", e.getMessage(), e);
                return null;
            }
        }

        @Override
        protected void onPostExecute(ArrayList<Product> products)
        {
            super.onPostExecute(products);
        }
    }

    public class DeletProduct extends AsyncTask<Void, Void, Void> {
        private Product product;

        DeletProduct(Product product)
        {
            this.product = product;
        }

        @Override
        protected Void doInBackground(Void... params) {
            ArrayList<Product> products;
            try {
                final String url = "http://10.0.2.2:8080/product";
                RestTemplate restTemplate = new RestTemplate();
                String entityUrl = url + "/" + product.getId();
                restTemplate.delete(entityUrl);

            } catch (Exception e) {
                Log.e("MainActivity", e.getMessage(), e);
            }
            return null;
        }
    }
}
