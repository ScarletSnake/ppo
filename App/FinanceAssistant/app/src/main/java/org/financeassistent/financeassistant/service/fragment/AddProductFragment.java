package org.financeassistent.financeassistant.service.fragment;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import androidx.fragment.app.Fragment;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.financeassistent.financeassistant.MainActivity;
import org.financeassistent.financeassistant.R;
import org.financeassistent.financeassistant.model.Product;
import org.financeassistent.financeassistant.service.db.ConnectionDB;
import org.springframework.http.HttpEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class AddProductFragment extends Fragment {
    private EditText productName;
    private EditText productPrice;
    private boolean productNecessary;
    private View view;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.addproduct_fragment, container, false);

        CheckBox isNecessary = (CheckBox) view.findViewById(R.id.isNecessary);
        CheckBox isnotNecessary = (CheckBox) view.findViewById(R.id.isnotNecessary);

        isNecessary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                productNecessary = true;
                CheckBox isnotNecessary = (CheckBox) view.findViewById(R.id.isnotNecessary);
                isnotNecessary.setChecked(false);
            }
        });

        isnotNecessary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                productNecessary = false;
                CheckBox isNecessary = (CheckBox) view.findViewById(R.id.isNecessary);
                isNecessary.setChecked(false);
            }
        });

        final Button setProduct = (Button) view.findViewById(R.id.setProduct_Button);
        setProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setProduct();
            }
        });

        return view;
    }

    public void setProduct(){
        productName = view.findViewById(R.id.productName);
        productPrice = view.findViewById(R.id.productPrice);

        Product product = new Product(productName.getText().toString(),
                Double.parseDouble(productPrice.getText().toString()),
                productNecessary);

        SetProduct setProduct = new SetProduct(product,
                ConnectionDB.connectionString,
                ConnectionDB.user,
                ConnectionDB.password);
        setProduct.execute();
    }

    public class SetProduct extends AsyncTask<Void, Void, Void> {
        private Product product;
        protected Connection conn = null;
        protected Statement st = null;
        String url;
        String user;
        String password;

        SetProduct(Product product, String url, String user, String password)
        {
            this.product = product;
            this.url = url;
            this.user = user;
            this.password = password;
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                final String url = "http://10.0.2.2:8080/product";
                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(0,
                        new StringHttpMessageConverter(Charset.forName("UTF-8")));

                HttpEntity<Product> request = new HttpEntity<Product>(product);
                restTemplate.postForObject(url, request, Product.class);

            } catch (Exception e) {
                Log.e("MainActivity", e.getMessage(), e);
            }
            return null;
        }
    }
}
