package org.financeassistent.financeassistant.model;

import android.os.Build;

import java.time.LocalDateTime;

public class User {
    private String id;
    private String name;
    private String surename;
    private String userpic;
    private String email;
    private String locale;
    private String lastVisit;
    private boolean isAdmin;

    public User()
    {

    }

    public User(String id, String name, String surename, String userpic,
                String email, String locale, String lastVisit,
                boolean isAdmin) {
        this.name = name;
        this.surename = surename;
        this.userpic = userpic;
        this.email = email;
        this.locale = locale;
        this.lastVisit = lastVisit;
        this.isAdmin = isAdmin;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurename() {
        return surename;
    }

    public void setSurename(String surename) {
        this.surename = surename;
    }

    public String getUserpick() {
        return userpic;
    }

    public void setUserpic(String userpic) {
        this.userpic = userpic;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getLastVisit() {
        return lastVisit;
    }

    public void setLastVisit(String lastVisit) {
        this.lastVisit =  lastVisit;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }

}
