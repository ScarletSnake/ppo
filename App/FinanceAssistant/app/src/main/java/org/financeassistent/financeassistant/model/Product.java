package org.financeassistent.financeassistant.model;

public class Product {
    private Long id;

    private String name;

    private double price;

    private boolean wasNecessary;

    private User user;

    public Product(){

    }

    public Product(String name, double price, boolean wasNecessary)
    {
        this.price = price;
        this.name = name;
        this.wasNecessary = wasNecessary;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public boolean getWasNecessary() {
        return wasNecessary;
    }

    public void setWasNecessary(boolean wasNecessary) {
        this.wasNecessary = wasNecessary;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
