package org.financeassistent.financeassistant.service.typechange.impl;

import org.financeassistent.financeassistant.service.typechange.ITypeChanger;

public class TypeChanger implements ITypeChanger {

    public String BoolToString(boolean example)
    {
        if(example)
            return "Да";
        else
            return "Нет";
    }

}
