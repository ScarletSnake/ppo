package app.controllers;


import app.repos.ProductRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("inform")
public class ProductInformController {
    private final ProductRepo prodRep;

    @Autowired
    public ProductInformController(ProductRepo prodRep) {
        this.prodRep = prodRep;
    }

    @GetMapping("/price")
    public float getTotalPrice()
    {
        return this.prodRep.getTotalPrice();
    }

    @GetMapping("/count")
    public int getCount()
    {
        return this.prodRep.getCount();
    }

    @GetMapping("/count/wasNecessary")
    public int getCountNecessary()
    {
        return this.prodRep.getCountNecessary();
    }

    @GetMapping("/count/wasNecessary/not")
    public int getCountNotNecessary()
    {
        return this.prodRep.getCountNotNecessary();
    }

    @GetMapping("/price/wasNecessary")
    public double getTotalPriceNecessary()
    {
        return this.prodRep.getTotalPriceNecessary();
    }

    @GetMapping("/price/wasNecessary/not")
    public double getTotalPriceNotNecessary()
    {
        return this.prodRep.getTotalPriceNotNecessary();
    }

    @GetMapping("/price/average")
    public float getAveragePrice()
    {
        return this.prodRep.getAveragePrice();
    }
}
