package app.controllers;

import app.models.Earning;
import app.models.Product;
import app.repos.EarningRepo;
import app.repos.ProductRepo;
import app.sorts.EarningSelectorSortBy;
import app.sorts.ProductSelectorSortBy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("sort")
public class SortController {
    private final EarningRepo earningRepo;
    private final ProductRepo productRepo;

    @Autowired
    public SortController(EarningRepo earningRepo, ProductRepo productRepo) {
        this.earningRepo = earningRepo;
        this.productRepo = productRepo;
    }

    @GetMapping("/sortProduct")
    @ResponseBody
    public List<Product> sortProduct(@RequestParam String orderingBy)
    {
        return ProductSelectorSortBy.selectSort(orderingBy, productRepo);
    }

    @GetMapping("/sortEarning")
    @ResponseBody
    public List<Earning> sortEarning(@RequestParam String orderingBy)
    {
        return EarningSelectorSortBy.selectSort(orderingBy, earningRepo);
    }
}
