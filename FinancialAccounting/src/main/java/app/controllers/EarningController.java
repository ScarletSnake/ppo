package app.controllers;

import app.models.Earning;
import app.models.Product;
import app.repos.EarningRepo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("earning")
public class EarningController {
    private final EarningRepo earningRepo;

//    @Autowired
    public EarningController(EarningRepo earningRepo) {
        this.earningRepo = earningRepo;
    }

    @GetMapping
    public List<Earning> list()
    {
        return earningRepo.findAll();
    }

    @GetMapping("{id}")
    public Earning getOne(@PathVariable("id") Earning earning)
    {
        return earning;
    }

    @PostMapping
    public Earning create(@RequestBody Earning earning)
    {
        return earningRepo.save(earning);
    }

    @PutMapping("{id}")
    public Earning update(@PathVariable("id") Earning earningFromDb, @RequestBody Product product)
    {
        BeanUtils.copyProperties(product, earningFromDb, "id");

        return earningRepo.save(earningFromDb);
    }

    @DeleteMapping("{id}")
    public void delete(@PathVariable("id") Earning earning)
    {
        earningRepo.delete(earning);
    }

}
