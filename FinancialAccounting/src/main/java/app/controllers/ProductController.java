package app.controllers;

import app.models.Product;
import app.repos.ProductRepo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("product")
public class ProductController {
    private final ProductRepo prodRep;

    @Autowired
    public ProductController(ProductRepo prodRep) {
        this.prodRep = prodRep;
    }

    @GetMapping
    public List<Product> list()
    {

        List<Product> products = prodRep.findAll();
        for (Product product: products) {
            product.setUser(null);
        }
        return products;
    }

    @GetMapping("{id}")
    public Product getOne(@PathVariable("id") Product product)
    {
        product.setUser(null);
        return product;
    }

    @PostMapping
    public Product create(@RequestBody Product product)
    {
        return prodRep.save(product);
    }

    @PutMapping("{id}")
    public Product update(@PathVariable("id") Product productFromDb, @RequestBody Product product)
    {
        BeanUtils.copyProperties(product, productFromDb, "id");

        return prodRep.save(productFromDb);
    }

    @DeleteMapping("{id}")
    public void delete(@PathVariable("id") Product product)
    {
        prodRep.delete(product);
    }
}