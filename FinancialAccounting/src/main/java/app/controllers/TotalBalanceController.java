package app.controllers;

import app.repos.EarningRepo;
import app.repos.ProductRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("balance")
public class TotalBalanceController {
    private final EarningRepo earningRepo;
    private final ProductRepo productRepo;

    @Autowired
    public TotalBalanceController(EarningRepo earningRepo, ProductRepo productRepo) {
        this.earningRepo = earningRepo;
        this.productRepo = productRepo;
    }

    @GetMapping()
    public float getBalance()
    {
        return earningRepo.getTotalEarn() - productRepo.getTotalPrice();
    }

}
