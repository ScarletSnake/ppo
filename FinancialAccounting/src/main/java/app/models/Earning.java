package app.models;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Table
@ToString(of = {"id", "type"})
@EqualsAndHashCode(of = {"id"})
public class Earning {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String type;

    private double earnValue;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="user_id")
    private User user;

    public Earning() {
    }

    public Earning(String type, double earnValue, User user)
    {
        this.type = type;
        this.earnValue = earnValue;
        this.user = user;
    }

    public Long getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public double getEarnValue() {
        return earnValue;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
