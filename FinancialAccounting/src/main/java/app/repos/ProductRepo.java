package app.repos;

import app.models.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ProductRepo extends JpaRepository<Product, Long> {

    @Query(
            value = "select * from product order by name",
            nativeQuery = true
    )
    List<Product> sortProductsByName();

    @Query(
            value = "select * from product order by price",
            nativeQuery = true
    )
    List<Product> sortProductsByPrice();

    @Query(
            value = "select * from product order by id",
            nativeQuery = true
    )
    List<Product> sortProductsById();

    @Query(
            value = "select sum(price) from product",
            nativeQuery = true
    )
    float getTotalPrice();

    @Query(
            value = "select count(*) from product",
            nativeQuery = true
    )
    int getCount();

    @Query(
            value = "select count(*) from product where was_necessary = true",
            nativeQuery = true
    )
    int getCountNecessary();

    @Query(
            value = "select count(*) from product where was_necessary != true",
            nativeQuery = true
    )
    int getCountNotNecessary();

    @Query(
            value = "select sum(price) from product where was_necessary = true",
            nativeQuery = true
    )
    double getTotalPriceNecessary();

    @Query(
            value = "select sum(price) from product where was_necessary != true",
            nativeQuery = true
    )
    double getTotalPriceNotNecessary();

    @Query(
            value = "select avg(price) from product",
            nativeQuery = true
    )
    float getAveragePrice();
}
