package app.repos;

import app.models.Earning;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface EarningRepo extends JpaRepository<Earning, Long> {
    @Query(
            value = "select * from earning order by type",
            nativeQuery = true
    )
    List<Earning> sortProductsByType();

    @Query(
            value = "select * from earning order by earn_value",
            nativeQuery = true
    )
    List<Earning> sortProductsByValue();

    @Query(
            value = "select * from earning order by id",
            nativeQuery = true
    )
    List<Earning> sortProductsById();

    @Query(
            value = "select sum(earn_value) from earning",
            nativeQuery = true
    )
    float getTotalEarn();
}
