package app.sorts;

import app.models.Earning;
import app.repos.EarningRepo;

import java.util.List;

public class EarningSelectorSortBy {
    public static List<Earning> selectSort(String orderBy, EarningRepo earningRepo)
    {
        switch (orderBy)
        {
            case "type": return earningRepo.sortProductsByType();
            case "earn_value": return earningRepo.sortProductsByValue();
            case "id": return earningRepo.sortProductsById();
            default: return null;
        }
    }
}
