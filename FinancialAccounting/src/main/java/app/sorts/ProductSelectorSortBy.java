package app.sorts;

import app.models.Product;
import app.repos.ProductRepo;

import java.util.List;

public class ProductSelectorSortBy {
    public static List<Product> selectSort(String orderBy, ProductRepo productRepo)
    {
        switch (orderBy)
        {
            case "name": return productRepo.sortProductsByName();
            case "price": return productRepo.sortProductsByPrice();
            case "id": return productRepo.sortProductsById();
            default: return null;
        }
    }
}
